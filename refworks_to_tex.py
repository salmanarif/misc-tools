# Install the BeautifulSoup package
# Edit all Author fields in RefWorks to contain the latex citation reference:
# for example, "Clarke, T.J.W.C." should become "@!cite_ref!@Clarke, T.J.W.C."
# Download the bibliography from RefWorks as an HTML file, using the
# "Imperial College Vancouver" Output Style.
# Run the script: refworks_to_tex.py <input> <output>
# Import the tex file into latex where you want your bibliography to be
# Cite entries using \cite{cite_ref}

from bs4 import BeautifulSoup
import re, sys, getopt

vargin = list(sys.argv)
nargin = len(sys.argv)
if nargin < 2:
    print "Did not provide input or output filename."
    print "Usage: refworks_to_tex.py <input> <output>"
    exit()
elif nargin < 3:
    print "Did not provide output filename."
    print "Usage: refworks_to_tex.py <input> <output>"
    exit()
else:
    infile = vargin[1]
    outfile = vargin[2]

refworks = open(infile,'r')
soup = BeautifulSoup(refworks)

para = soup.findAll('p')
string = ''
for s in para:
    if not str(s) in string:
        string = string + str(s)

fixed = '\\begin{thebibliography}{99}'
fixed = fixed + re.sub('<p style.*?\)','\\\\bibitem',string)
fixed = re.sub('<font.*?\>','',fixed)
fixed = fixed.replace('</font>','')
fixed = fixed.replace('<br>','')
fixed = fixed.replace('</br>','')
fixed = fixed.replace(' @!','{')
fixed = fixed.replace('!@','} ')
fixed = fixed.replace('<i>','\\textit{')
fixed = fixed.replace('</i>','}')
fixed = fixed.replace('<I>','\\textit{')
fixed = fixed.replace('</I>','}')
fixed = fixed.replace('<B>','')
fixed = fixed.replace('</B>','')
fixed = fixed.replace('<b>','')
fixed = fixed.replace('</b>','')
fixed = fixed.replace('[','{[}')
fixed = fixed.replace(']','{]}')
fixed = re.sub('<a href=".*?target="_blank">','\\url{',fixed)
fixed = fixed.replace('</a>','}')
fixed = fixed.replace('&amp;','\\&')
fixed = fixed.replace('%','\%')
fixed = fixed.replace('_','\_')
fixed = fixed.replace('<93>','``')
fixed = fixed.replace('<94>','"')

# do these last
fixed = fixed.replace('<p style="line-height: 1"></p>','')
fixed = fixed.replace('</p>','')
fixed = fixed.replace('<p align="center">References','')
fixed = fixed.replace('<p align="center">','')
fixed = fixed.replace('<p style="line-height: 1">','')
fixed = fixed + '\\end{thebibliography}'

f = open(outfile,'w')
f.write(fixed)
f.close()
