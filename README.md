Random tools and stuff made to solve minor inconveniences.

## **Contents** ##
`convert_to_tex.py` Python script to convert a RefWorks bibliography to LaTeX code for `thebibliography` package.